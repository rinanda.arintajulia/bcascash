package com.example.bcascash.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class OfferingModel (
    val image: Int?,
    val title: String?,
    val subTitle: String?
) : Parcelable