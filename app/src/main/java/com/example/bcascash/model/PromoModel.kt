package com.example.bcascash.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class PromoModel (
    val image: Int?,
    val title: String?,
    val subTitle: String?
) : Parcelable