package com.example.bcascash.view.login

import android.accounts.AccountManager.KEY_PASSWORD
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.view.register.RegisterActivity
import com.example.bcascash.databinding.ActivityLoginBinding
import com.example.bcascash.sharedPref.Companion
import com.example.bcascash.sharedPref.SharedPref
import com.example.bcascash.view.dashboard.DashboardActivity

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    lateinit var sharedPref: SharedPref
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = SharedPref(this)

        val email = sharedPref.getString(Companion.KEY_EMAIl)
        val password = sharedPref.getString(Companion.KEY_PASS)


        binding.btnLogin.setOnClickListener{
            val inputEmail = binding.etEmail.text.toString()
            val inputPassword = binding.etPassword.text.toString()

            if (inputEmail == email && inputPassword == password){
                startActivity(Intent(this, DashboardActivity::class.java))
                finish()
            } else {
                Toast.makeText(this, "Masukkan Email dan Password", Toast.LENGTH_LONG).show()
            }

        }
        binding.tvHaventAccount.setOnClickListener{
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }
}