package com.example.bcascash.view.firstpage

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.databinding.ActivityFirstBinding
import com.example.bcascash.view.main.MainActivity

class FirstActivity : AppCompatActivity() {
    private val TIME_OUT:Long = 3000
    private lateinit var binding: ActivityFirstBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFirstBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navigateToMain(MainActivity::class.java)
    }

    private fun navigateToMain(main: Class<*>) {
        val intent = Intent(applicationContext, main)
        Handler().postDelayed({
            startActivity(intent)
            finish()
        }, TIME_OUT)


    }
}