package com.example.bcascash.view.promo

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.R
import com.example.bcascash.databinding.ActivityPromoBinding
import com.example.bcascash.model.PromoModel
import com.example.bcascash.view.dashboard.DashboardActivity
import com.example.bcascash.view.adapter.OfferingAdapter
import com.example.bcascash.view.adapter.PromoAdapter

class PromoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPromoBinding
    private val promoAdapter = PromoAdapter()

    private val offeringAdapter = OfferingAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPromoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvPromo.adapter = promoAdapter
        binding.rvOffering.adapter = offeringAdapter

        promoAdapter.addDataPromo(populateData2())
        offeringAdapter.addDataOffering(populateData())

        promoAdapter.addOnClickPromo { dataPromo ->
            DetailPromoActivity.navigateToActivityDetail(
                this, dataPromo
            )
        }

        binding.componentAppBar.ivBack.setOnClickListener{
            startActivity(Intent(this, DashboardActivity::class.java))}

//        val mainAdapter = PromoAdapter(
//            dataPromo = populateData(),
//            onClickPromo = {
//                DetailPromoActivity.navigateToActivityDetail(this, it)
//
//            })
//
//        val data = populateData()
//        mainAdapter.addDataPromo(data)
//
//        mainAdapter.addOnClickPromo { dataNews ->
//            DetailPromoActivtiy.navigateToActivityDetail(
//                this, dataNews
//            )

    }

    private fun populateData2(): MutableList<PromoModel> {
        return mutableListOf(
            PromoModel(
                image = R.drawable.ic_promo,
                title = "Lorem ipsum dolor sit amet",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            PromoModel(
                image = R.drawable.ic_promo2,
                title = "Lorem ipsum dolor sit amet",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            PromoModel(
                image = R.drawable.ic_promo3,
                title = "Lorem ipsum dolor sit amet",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            )
        )
    }


    private fun populateData(): List<ItemOffering> {
        val listData = listOf(
            ItemOffering(
                image = R.drawable.ic_offering,
                title = "Lorem ipsum dolor sit amet",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            ItemOffering(
                image = R.drawable.ic_offering2,
                title = "Lorem ipsum dolor sit amet",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            ItemOffering(
                image = R.drawable.ic_offering3,
                title = "Lorem ipsum dolor sit amet",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            )
        )
        return listData

    }

}
