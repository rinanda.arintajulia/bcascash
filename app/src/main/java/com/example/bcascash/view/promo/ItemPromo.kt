package com.example.bcascash.view.promo

data class ItemPromo(
    val image: Int?,
    val title: String?,
    val subTitle: String?
)