package com.example.bcascash.view.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.databinding.ActivityRegisterBinding
import com.example.bcascash.sharedPref.Companion
import com.example.bcascash.sharedPref.Companion.Companion.KEY_NAME
import com.example.bcascash.sharedPref.SharedPref
import com.example.bcascash.view.login.LoginActivity

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    lateinit var sharedPref: SharedPref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = SharedPref(this)

        binding.btnRegister.setOnClickListener{
            val inputName = binding.etFullname.text.toString()
            val inputEmail = binding.etEmail.text.toString()
            val inputNumber = binding.etNumber.text.toString()
            val inputAddress = binding.etAddress.text.toString()
            val inputPassword = binding.etPassword.text.toString()

            if (inputName.isNullOrEmpty() || inputEmail.isNullOrEmpty() || inputNumber.isNullOrEmpty() || inputAddress.isNullOrEmpty()) {
                Toast.makeText(applicationContext, "Input Data Tidak Boleh Kosong", Toast.LENGTH_LONG).show()
            } else {
                sharedPref.put(Companion.KEY_NAME, inputName)
                sharedPref.put(Companion.KEY_EMAIl, inputEmail)
                sharedPref.put(Companion.KEY_NUMBER, inputNumber)
                sharedPref.put(Companion.KEY_ADDRESS, inputAddress)
                sharedPref.put(Companion.KEY_PASS, inputPassword)

                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }

        }
        binding.tvHaventAccount.setOnClickListener{
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

}