package com.example.bcascash.view.promo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.databinding.ActivityPromoBinding

class OfferingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPromoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPromoBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}