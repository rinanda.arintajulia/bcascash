package com.example.bcascash.view.promo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.databinding.ActivityDetailPromoBinding
import com.example.bcascash.model.PromoModel

class DetailPromoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailPromoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailPromoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDataToViewDetail()

    }

    private fun setDataToViewDetail() {
        val data = intent.getParcelableExtra<PromoModel>(DATA_PROMO)
        binding.ivDetailPromo.setImageResource(data?.image ?: 0)
        binding.tvTitlePromo.text = data?.title
        binding.tvSubtitlePromo.text = data?.subTitle

        binding.componentAppBar.tvAppbar.text = data?.title
        binding.componentAppBar.ivBack.setOnClickListener{
            this.onBackPressed()
        }

    }

    companion object {
        private const val DATA_PROMO = "dataPromo"
        fun navigateToActivityDetail(
            activity: Activity, dataPromo: PromoModel
        ) {
            val intent = Intent(activity, DetailPromoActivity::class.java)
            intent.putExtra(DATA_PROMO, dataPromo)
            activity.startActivity(intent)
        }
    }
}