package com.example.bcascash.view.about

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.databinding.ActivityAboutBinding
import com.example.bcascash.view.dashboard.DashboardActivity

class AboutActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAboutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAboutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.componentAppBar.ivBack.setOnClickListener{
            startActivity(Intent(this, DashboardActivity::class.java))
        }
    }
}