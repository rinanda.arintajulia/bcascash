package com.example.bcascash.view.profile

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.databinding.ActivityProfileBinding
import com.example.bcascash.sharedPref.Companion.Companion.KEY_ADDRESS
import com.example.bcascash.sharedPref.Companion.Companion.KEY_EMAIl
import com.example.bcascash.sharedPref.Companion.Companion.KEY_NAME
import com.example.bcascash.sharedPref.Companion.Companion.KEY_NUMBER
import com.example.bcascash.sharedPref.Companion.Companion.KEY_PASS
import com.example.bcascash.sharedPref.SharedPref
import com.example.bcascash.view.dashboard.DashboardActivity

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding
    lateinit var sharedPref: SharedPref
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = SharedPref(this)

        val name = sharedPref.getString(KEY_NAME)
        val email = sharedPref.getString(KEY_EMAIl)
        val number = sharedPref.getString(KEY_NUMBER)
        val address = sharedPref.getString(KEY_ADDRESS)
        val password = sharedPref.getString(KEY_PASS)

        binding.tvName.text = name
        binding.tvEmailBio.text = email
        binding.tvPhoneNumber.text = number
        binding.tvLocation.text = address

        binding.componentAppBar.ivBack.setOnClickListener{
            startActivity(Intent(this, DashboardActivity::class.java))
        }
    }
}