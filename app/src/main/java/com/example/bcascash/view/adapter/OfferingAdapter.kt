package com.example.bcascash.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bcascash.view.promo.ItemOffering
import com.example.bcascash.databinding.ItemOfferingBinding

class OfferingAdapter :
    RecyclerView.Adapter<OfferingAdapter.HomeMainViewholder>() {

    private val dataOffering: MutableList<ItemOffering> =  mutableListOf()
    private var onClickOffering: (ItemOffering) -> Unit = {}

    fun addDataOffering(newData: List<ItemOffering>) {
        dataOffering.addAll(newData)
        notifyDataSetChanged()
    }

    fun addOnClickOffering(clickOffering: (ItemOffering) -> Unit){
        onClickOffering = clickOffering
    }

    inner class HomeMainViewholder(val binding: ItemOfferingBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data: ItemOffering, onClickNews: (ItemOffering) -> Unit) {
            binding.ivItemOffering.setImageResource(data.image ?: 0)
            binding.tvTitleOffering.text = data.title
            binding.tvSubtitleOffering.text = data.subTitle

            binding.llOffering.setOnClickListener{
                onClickNews(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeMainViewholder
            = HomeMainViewholder(
        ItemOfferingBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

    )

    override fun onBindViewHolder(holder: HomeMainViewholder, position: Int) {
        holder.bindView(dataOffering[position], onClickOffering)
    }

    override fun getItemCount(): Int = dataOffering.size

}