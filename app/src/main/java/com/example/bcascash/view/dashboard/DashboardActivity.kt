package com.example.bcascash.view.dashboard

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.databinding.ActivityDashboardBinding
import com.example.bcascash.sharedPref.SharedPref
import com.example.bcascash.view.about.AboutActivity
import com.example.bcascash.view.faq.FaqActivity
import com.example.bcascash.view.main.MainActivity
import com.example.bcascash.view.profile.ProfileActivity
import com.example.bcascash.view.promo.PromoActivity

class DashboardActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDashboardBinding
    lateinit var sharedPref: SharedPref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = SharedPref(this)

        binding.layoutProfile.setOnClickListener{
            startActivity(Intent(this, ProfileActivity::class.java))
        }
        binding.layoutAbout.setOnClickListener{
            startActivity(Intent(this, AboutActivity::class.java))
        }
        binding.layoutPromo.setOnClickListener{
            startActivity(Intent(this, PromoActivity::class.java))
        }
        binding.layoutFaq.setOnClickListener{
            startActivity(Intent(this, FaqActivity::class.java))
        }
        binding.btnLogout.setOnClickListener{
            sharedPref.clear()
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        }
    }
}