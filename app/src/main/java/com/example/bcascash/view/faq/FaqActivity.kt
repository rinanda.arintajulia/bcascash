package com.example.bcascash.view.faq

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bcascash.databinding.ActivityFaqBinding
import com.example.bcascash.view.dashboard.DashboardActivity

class FaqActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFaqBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFaqBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.componentAppBar.ivBack.setOnClickListener{
            startActivity(Intent(this, DashboardActivity::class.java))
        }
    }
}