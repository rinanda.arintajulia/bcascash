package com.example.bcascash.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bcascash.databinding.ItemPromoBinding
import com.example.bcascash.model.PromoModel
import com.example.bcascash.view.promo.ItemPromo

class PromoAdapter :
    RecyclerView.Adapter<PromoAdapter.HomeMainViewholder>() {

    private val dataPromo: MutableList<PromoModel> =  mutableListOf()
    private var onClickPromo: (PromoModel) -> Unit = {}

    fun addDataPromo(newData: List<PromoModel>) {
        dataPromo.addAll(newData)
        notifyDataSetChanged()
    }

    fun addOnClickPromo(clickPromo: (PromoModel) -> Unit){
        onClickPromo = clickPromo
    }

    inner class HomeMainViewholder(val binding: ItemPromoBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data: PromoModel, onClickNews: (PromoModel) -> Unit) {
            binding.ivItemPromo.setImageResource(data.image ?: 0)
            binding.tvTitlePromo.text = data.title
            binding.tvSubtitlePromo.text = data.subTitle

            binding.llPromo.setOnClickListener{
                onClickNews(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeMainViewholder
            = HomeMainViewholder(
        ItemPromoBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

    )

    override fun onBindViewHolder(holder: HomeMainViewholder, position: Int) {
        holder.bindView(dataPromo[position], onClickPromo)
    }

    override fun getItemCount(): Int = dataPromo.size

}