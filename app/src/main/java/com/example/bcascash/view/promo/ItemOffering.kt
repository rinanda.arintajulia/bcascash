package com.example.bcascash.view.promo

data class ItemOffering(
    val image: Int?,
    val title: String?,
    val subTitle: String?
)