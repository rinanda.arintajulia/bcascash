package com.example.bcascash.sharedPref

class Companion {
    companion object {
        val KEY_LOGIN = "isLogin"
        val KEY_NAME = "name"
        val KEY_NUMBER = "number"
        val KEY_PASS = "password"
        val KEY_ADDRESS = "address"
        val KEY_EMAIl = "email"
    }
}